package ru.qqqppp.leo.mvvmapplication

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

/**
 * Main view model with text
 *
 * @author Leonid Emelyanov
 */
class MainViewModel : ViewModel() {

    private var count = 0

    val text = MutableLiveData<String>()
    val isVisible = MutableLiveData<Boolean>()

    init {
        text.value = count.toString()

        launch {
            delay(2000)
            isVisible.postValue(true)
        }
    }

    fun onClick() {
        text.postValue((++count).toString())
    }

    fun onChangeVisibility() {
        isVisible.value = isVisible.value?.not()
    }
}