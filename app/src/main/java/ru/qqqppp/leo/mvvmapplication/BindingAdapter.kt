package ru.qqqppp.leo.mvvmapplication

import android.databinding.BindingAdapter
import android.view.View

/**
 * Data binding adapter
 *
 * @author Leonid Emelyanov
 */

@BindingAdapter("visibility")
fun setVisibility(view: View,
                  isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}