package ru.qqqppp.leo.mvvmapplication

import android.databinding.BaseObservable
import android.databinding.Bindable

class ClassicViewModel : BaseObservable() {

    private var mCount = 0
    private var mText = ""
    private var mIsVisible = true

    @Bindable
    fun getText() = mText

    @Bindable
    fun getIsVisible() = mIsVisible

    private fun setText(text: String) {
        mText = text
        notifyPropertyChanged(BR.text)
    }

    private fun setIsVisible(isVisible: Boolean) {
        mIsVisible = isVisible
        notifyPropertyChanged(BR.isVisible)
    }

    fun onClick() {
        setText((++mCount).toString())
    }

    fun onChangeVisibility() {
        mIsVisible = mIsVisible.not()
        setIsVisible(mIsVisible)
    }
}