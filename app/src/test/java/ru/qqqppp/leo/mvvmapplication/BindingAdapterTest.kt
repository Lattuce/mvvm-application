package ru.qqqppp.leo.mvvmapplication

import android.view.View
import com.nhaarman.mockito_kotlin.mock
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * @author Leonid Emelyanov
 */
@RunWith(MockitoJUnitRunner::class)
class BindingAdapterTest {

    private val view: View = mock()

    @Test
    fun setVisibleTest() {
        setVisibility(view, true)
        Mockito.verify(view).visibility = eq(View.VISIBLE)
    }

    @Test
    fun setGoneTest() {
        setVisibility(view, false)
        Mockito.verify(view).visibility = eq(View.GONE)
    }
}