package ru.qqqppp.leo.mvvmapplication

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * @author Leonid Emelyanov
 */
@RunWith(JUnit4::class)
class MainViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel

    private val textObserver: Observer<String> = mock()
    private val visibilityObserver: Observer<Boolean> = mock()

    @Before
    fun setUp() {
        viewModel = MainViewModel()
    }

    @Test
    fun getText() {
        viewModel.text.observeForever(textObserver)
        viewModel.text.postValue("")

        verify(textObserver).onChanged("")
    }

    @Test
    fun isVisible() {
        viewModel.isVisible.observeForever(visibilityObserver)
        viewModel.isVisible.postValue(false)

        verify(visibilityObserver).onChanged(false)
    }

    @Test
    fun onClickTest() {
        viewModel.text.observeForever(textObserver)
        viewModel.onClick()

        verify(textObserver).onChanged("1")
    }

    @Test
    fun onChangeVisibilityTest() {
        viewModel.isVisible.observeForever(visibilityObserver)
        viewModel.onChangeVisibility()

        verify(visibilityObserver).onChanged(false)
    }
}